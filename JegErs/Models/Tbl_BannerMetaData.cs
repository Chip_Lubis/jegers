﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class Tbl_BannerMetaData
    {
        [Display(Name = "Banner Number")]
        [DataType(DataType.Text)]
        public int Id_Banner { get; set; }

        [Display(Name = "File Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Display(Name = "File Type")]
        [DataType(DataType.Text)]
        public string ContentType { get; set; }

        [Display(Name = "Image Data")]
        [DataType(DataType.Text)]
        public byte[] Data { get; set; }

        [Display(Name = "Activate Banner?")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Banner active or not required")]
        [DataType(DataType.Text)]
        public string Active_Banner { get; set; }
    }
}