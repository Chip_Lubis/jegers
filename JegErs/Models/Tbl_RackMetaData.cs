﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class Tbl_RackMetaData
    {
        [Display(Name = "Rack Number")]
        [DataType(DataType.Text)]
        public int Id_Rack { get; set; }

        [Display(Name = "Rack Location")]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use alphabet characters only")]
        [MaxLength(1, ErrorMessage = "Input only 1 alphabet characters")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Rack Name required")]
        public string Rack_location { get; set; }
    }
}