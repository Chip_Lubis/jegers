﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class DetailsOrderModel
    {
        [Display(Name = "Order Number")]
        public int IDorder { get; set; }

        [Display(Name = "Order Date")]
        public DateTime? Order_date { get; set; }

        public string Status { get; set; }

        public string Delivery { get; set; }

        [Display(Name = "Total Price")]
        public int Total_price { get; set; }

        [Display(Name = "Clothes Weight")]
        public int Weight_Kg { get; set; }

        [Display(Name = "Kg Price")]
        public int? Kg_price { get; set; }

        [Display(Name = "Special Price")]
        public int? Special_price { get; set; }

        [Display(Name = "Delivery Price")]
        public int? Delivery_Price { get; set; }

        public List<Tbl_Itemdetail> ObjItem { get; set; }
    }
}