﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JegErs.Models
{
    public class JoinItemMeasure
    {
        public Tbl_Item ModelItem { get; set; }

        public Tbl_Measure ModelMeasure { get; set; }
    }
}