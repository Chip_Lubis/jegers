﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class Tbl_OrderdetailMetaData
    {
        public int IDitemdetail { get; set; }
        public int IDorder { get; set; }

        [Display(Name = "Total Price Kilogram")]
        [DataType(DataType.Text)]
        public Nullable<int> Kg_Price { get; set; }

        [Display(Name = "Total Price Special Clothes")]
        [DataType(DataType.Text)]
        public Nullable<int> Total_Special_Price { get; set; }

        [Display(Name = "Delivery Price")]
        [DataType(DataType.Text)]
        public Nullable<int> Delivery_Price { get; set; }

        [Display(Name = "Customer Number")]
        [DataType(DataType.Text)]
        public Nullable<int> IDcustlist { get; set; }
    }
}