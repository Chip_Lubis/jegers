﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class Tbl_ExpenseMetaData
    {
        [Display(Name = "Expense Number")]
        [DataType(DataType.Text)]
        public int id_Expense { get; set; }

        [Display(Name = "Expense Information")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Expense Information required")]
        [DataType(DataType.Text)]
        public string Expense_Info { get; set; }

        [Display(Name = "Amount")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter positive number")]
        public Nullable<int> Amount { get; set; }

        [Display(Name = "Total Expense")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Total Expense required")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter positive number")]
        public Nullable<int> Total_Expense { get; set; }

        [Display(Name = "Expense Date")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Expense Date required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public Nullable<System.DateTime> Expense_Date { get; set; }
    }
}