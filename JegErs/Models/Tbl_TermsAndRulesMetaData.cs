﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JegErs.Controllers
{
    public class Tbl_TermsAndRulesMetaData
    {
        [Display(Name = "No")]
        public int id_termsandrules { get; set; }

        [Display(Name = "Information")]
        [DataType(DataType.Text)]
        public string info_terms_rules { get; set; }

        [Display(Name = "Image Info")]
        [DataType(DataType.Text)]
        public byte[] image_terms_rules { get; set; }
    }
}