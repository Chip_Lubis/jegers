﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JegErs.Controllers
{
    public class Tbl_OrderMetaData
    {
        [Display(Name = "Order Number")]
        [DataType(DataType.Text)]
        public int IDorder { get; set; }

        [Display(Name = "Customer Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Customer Name required")]
        [DataType(DataType.Text)]
        public string Customer_Name { get; set; }

        [Display(Name = "Delivery?")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Delivery or not required")]
        [DataType(DataType.Text)]
        public string Delivery { get; set; }

        [Display(Name = "Status")]
        [DataType(DataType.Text)]
        public string Status { get; set; }

        [Display(Name = "Already Pay?")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pay first or not required")]
        [DataType(DataType.Text)]
        public string Paid_Already { get; set; }

        [Display(Name = "Order Date")]
        [DataType(DataType.DateTime)]
        public Nullable<System.DateTime> Order_date { get; set; }

        [Display(Name = "Order Finish")]
        [DataType(DataType.DateTime)]
        public Nullable<System.DateTime> Order_finish { get; set; }

        [Display(Name = "Total Price")]
        [DataType(DataType.Text)]
        public Nullable<int> Total_Price { get; set; }

        [Display(Name = "Transfer Evidence")]
        [DataType(DataType.Text)]
        public byte[] Transfer_Evidence { get; set; }

        [Display(Name = "Rack Number")]
        [DataType(DataType.Text)]
        public Nullable<int> Id_Rack { get; set; }

        [Display(Name = "Location")]
        [DataType(DataType.Text)]
        public string Location { get; set; }

        [Display(Name = "Address")]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [Display(Name = "Admin Name")]
        [DataType(DataType.Text)]
        public string Admin_Name { get; set; }

        [Display(Name = "Bank Name")]
        [DataType(DataType.Text)]
        public string Bank_Name { get; set; }

        [Display(Name = "Bank Account Number")]
        [DataType(DataType.Text)]
        public Nullable<int> Bank_Account_Number { get; set; }

        [Display(Name = "Bank Customer Name")]
        [DataType(DataType.Text)]
        public string Bank_Customer_Name { get; set; }

        [Display(Name = "Phone Number")]
        [DataType(DataType.Text)]
        public string Phone_Number { get; set; }

        [Display(Name = "Customer Email")]
        [DataType(DataType.Text)]
        public string Email { get; set; }
    }
}