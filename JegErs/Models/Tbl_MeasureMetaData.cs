﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class Tbl_MeasureMetaData
    {
        [Display(Name = "Measure")]
        [DataType(DataType.Text)]
        public string Measure1 { get; set; }
    }
}