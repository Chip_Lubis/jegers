﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Controllers
{
    public class Tbl_ItemMetaData
    {
        [Display(Name = "Item Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Item Name required")]
        [DataType(DataType.Text)]
        public string Item_Name { get; set; }

        [Display(Name = "Measure")]
        [DataType(DataType.Text)]
        public string Id_Measure { get; set; }

        [Display(Name = "Laundry Price")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter positive number")]
        public string Laundry_Price { get; set; }

        [Display(Name = "Dry Clean Price")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter positive number")]
        public string DryClean_Price { get; set; }

    }
}