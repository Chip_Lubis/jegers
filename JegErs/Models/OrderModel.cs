﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class OrderModel
    {
        public Tbl_Order ModelOrder { get; set; }
        public Tbl_Orderdetail ModelOrderdetail { get; set; }
        public Tbl_Itemdetail ModelItemdetail { get; set; }
        public Tbl_Rack ModelRack { get; set; }

        public Tbl_Item ModelItem { get; set; }
        public Tbl_Measure ModelMeasure { get; set; }


        [Display(Name = "Input Clothes Weight (KG)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Input Clothes Weight")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter positive number")]
        public int? Weight_KG { get; set; }

        [Display(Name = "Delivery Price")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Input Delivery Price")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter positive number")]
        public int Delivery_Price { get; set; }

        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Input Address Customer")]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        public List<Tbl_Itemdetail> ObjItem { get; set; }

    }
}