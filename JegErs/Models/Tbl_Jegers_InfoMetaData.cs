﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Controllers
{
    public class Tbl_Jegers_InfoMetaData
    {
        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Address required")]
        [DataType(DataType.Text)]
        public string Address { get; set; }

        [Display(Name = "Phone Number")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Phone number required")]
        [DataType(DataType.Text)]
        public string Phone_Number { get; set; }

        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Link Social Media 1")]
        [DataType(DataType.Text)]
        public string Socmed_1 { get; set; }

        [Display(Name = "Link Social Media 2")]
        [DataType(DataType.Text)]
        public string Socmed_2 { get; set; }

        [Display(Name = "Link Social Media 3")]
        [DataType(DataType.Text)]
        public string Socmed_3 { get; set; }
    }
}