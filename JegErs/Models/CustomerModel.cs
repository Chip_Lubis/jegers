﻿using JegErs.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class CustomerModel
    {
        
        public Tbl_Custacc ModelAcc { get; set; }

        [Display(Name = "Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name required")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Address required")]
        [DataType(DataType.Text)]
        public string Address { get; set; }

        [Display(Name = "Phone Number")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Phone Number required")]
        [DataType(DataType.Text)]
        [MinLength(6, ErrorMessage = "Minimum 6 characters required")]
        public string Phone_Number { get; set; }
    }
}