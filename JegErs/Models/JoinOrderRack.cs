﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JegErs.Models
{
    public class JoinOrderRack
    {
        public Tbl_Order ModelOrder { get; set; }

        public Tbl_Rack ModelRack { get; set; }
    }
}