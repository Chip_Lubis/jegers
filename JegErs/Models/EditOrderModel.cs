﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class EditOrderModel
    {
        public int IDorder { get; set; }
        public int IDitemdetail { get; set; }

        [Display(Name = "Customer Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Customer Name required")]
        [DataType(DataType.Text)]
        public string Customer_Name { get; set; }

        [Display(Name = "Delivery?")]
        [DataType(DataType.Text)]
        public string Delivery { get; set; }

        public string Status { get; set; }

        [Display(Name = "Total KG Price")]
        [DataType(DataType.Text)]
        public Nullable<int> Kg_Price { get; set; }

        [Display(Name = "Total Special Price")]
        [DataType(DataType.Text)]
        public Nullable<int> Total_Special_Price { get; set; }

        [Display(Name = "Phone Number")]
        [DataType(DataType.Text)]
        public string Phone_Number { get; set; }
        [Display(Name = "Customer Email")]
        [DataType(DataType.Text)]
        public string Email { get; set; }

        [Display(Name = "Already Pay?")]
        [DataType(DataType.Text)]
        public string Paid_Already { get; set; }

        [Display(Name = "Rack Number")]
        [DataType(DataType.Text)]
        public Nullable<int> Id_Rack { get; set; }
        public Tbl_Itemdetail ModelItemdetail { get; set; }

        [Display(Name = "Input Clothes Weight (KG)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Input Clothes Weight")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter positive number")]
        public int Weight_KG { get; set; }

        [Display(Name = "Delivery Price")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Input Delivery Price")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter positive number")]
        public int? Delivery_Price { get; set; }

        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Input Address Customer")]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        public List<Tbl_Itemdetail> ObjItem { get; set; }
    }
}