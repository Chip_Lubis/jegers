﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Models
{
    public class Tbl_ItemdetailMetaData
    {
        public int IDorderitemdetail { get; set; }
        public int IDorder { get; set; }
        public int IDitemdetail { get; set; }

        [Display(Name = "Item Name")]
        [DataType(DataType.Text)]
        public string Item_Name { get; set; }

        [Display(Name = "Measure")]
        [DataType(DataType.Text)]
        public string Measure { get; set; }

        [Display(Name = "Amount")]
        [DataType(DataType.Text)]
        public Nullable<int> Amount { get; set; }

        [Display(Name = "Service Type")]
        [DataType(DataType.Text)]
        public string Servicetype { get; set; }

        [Display(Name = "Item Number")]
        [DataType(DataType.Text)]
        public Nullable<int> Id_Item { get; set; }

    }
}