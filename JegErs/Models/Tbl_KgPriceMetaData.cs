﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Controllers
{
    public class Tbl_KgPriceMetaData
    {
        [Display(Name = "Per Kg Price")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "KG price required")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter positive number")]
        public int Per_Kg_Price { get; set; }
    }
}