﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JegErs.Controllers
{
    public class Tbl_CustlistMetaData
    {
        [Display(Name = "Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Display(Name = "Address")]
        [DataType(DataType.Text)]
        public string Address { get; set; }

        [Display(Name = "Phone Number")]
        [DataType(DataType.Text)]
        public string Phone_Number { get; set; }

    }
}