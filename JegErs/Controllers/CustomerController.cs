﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.DynamicData;
using System.IO;
using JegErs.Models;
using System.Xml;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;

using System.Data.Entity.Validation;

namespace JegErs.Controllers
{
    public class CustomerController : Controller
    {
        JegErsContext db = new JegErsContext();
        // GET: Customer
        public ActionResult TermsAndRules()
        {
            return View();
        }

        public ActionResult DetailsOrder(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();

            DetailsOrderModel order = new DetailsOrderModel
            {
                IDorder = dataOrder.IDorder,
                Order_date = dataOrder.Order_date,
                Status = dataOrder.Status,
                Delivery = dataOrder.Delivery,
                Weight_Kg = Convert.ToInt32(dataOrderdetail.Kg_Price) / 5000,
                Kg_price = dataOrderdetail.Kg_Price,
                Special_price = dataOrderdetail.Total_Special_Price,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Total_price = Convert.ToInt32(dataOrderdetail.Kg_Price) + Convert.ToInt32(dataOrderdetail.Total_Special_Price) + Convert.ToInt32(dataOrderdetail.Delivery_Price),
                ObjItem = dataItemdetail
            };
            return View(order);
        }

        [HttpGet]
        public ActionResult Order()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Order(OrderModel model)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    string rack = Convert.ToString(model.ModelOrder.Customer_Name[0]);

                    var idrack = db.Tbl_Rack.Where(x => x.Rack_location == rack).FirstOrDefault();

                    Tbl_Order order = new Tbl_Order()
                    {
                        Customer_Name = model.ModelOrder.Customer_Name,
                        Delivery = model.ModelOrder.Delivery,
                        Phone_Number = model.ModelOrder.Phone_Number,
                        Address = model.ModelOrder.Address,
                        Email = model.ModelOrder.Email,
                        Order_date = DateTime.Now,
                        Location = "Online",
                        Id_Rack = Convert.ToInt32(idrack.Id_Rack),
                        Paid_Already = "No"
                    };
                    db.Tbl_Order.Add(order);
                    db.SaveChanges();

                    var idorder = order.IDorder;

                    Tbl_Orderdetail odetail = new Tbl_Orderdetail()
                    {
                        IDorder = idorder,
                        IDcustlist = Convert.ToInt32(Session["CustId"])
                    };
                    db.Tbl_Orderdetail.Add(odetail);
                    db.SaveChanges();

                    ts.Commit();

                    //email notif here
                    SendNotificationEmail("Order");
                    string message = "Order Requested";
                    TempData["message"] = message;
                    return RedirectToAction("History", "Customer");
                }
                catch (DbEntityValidationException)
                {
                    ts.Rollback();
                    throw;
                }
            }
        }

        [NonAction]
        public void SendNotificationEmail(string Notification)
        {
            var fromEmail = new MailAddress("jegerslaundry@gmail.com", "JegErs Laundry");
            var toEmail = new MailAddress("jegerslaundry@gmail.com");
            var fromEmailPassword = "RajaLaundry1"; // Replace with actual password

            string subject = "";
            string body = "";
            if (Notification == "Order")
            {
                subject = "New Online Order Requested!!";
                body = "A new Order from Customer has arrived.<br/><br/>Please process & contact the Customer immediately.";
            }
            else if (Notification == "Pay")
            {
                subject = "Customer has Upload Transfer Evidence!!";
                body = "A Customer has upload transfer evidence.<br/><br/>Please review evidence & confirm it with the Customer immediately.";
            }

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        public ActionResult History()
        {
            var id = Convert.ToInt32(Session["CustId"]);
            var order = (from a in db.Tbl_Order
                         join b in db.Tbl_Orderdetail on a.IDorder equals b.IDorder
                         join c in db.Tbl_Itemdetail on b.IDitemdetail equals c.IDitemdetail into table1
                         from c in table1.ToList().DefaultIfEmpty()
                         where b.IDcustlist == id
                         select new OrderModel
                         {
                             ModelOrder = a,
                             ModelOrderdetail = b,
                             ModelItemdetail = c
                         });
            ViewBag.Message = TempData["message"];
            return View(order.OrderByDescending(x => x.ModelOrder.IDorder));
        }


        [HttpGet]
        public ActionResult SettingAccount()
        {
            string username = Session["UserId"].ToString();
            Tbl_Custlist tbl_Custlist = db.Tbl_Custlist.Where(x => x.usernamecust == username).FirstOrDefault();
            return View(tbl_Custlist);
        }

        [HttpPost]
        public ActionResult SettingAccount(Tbl_Custlist tbl_Custlist)
        {
            using (DbContextTransaction ct = db.Database.BeginTransaction())
            {
                try
                {
                    if (tbl_Custlist != null)
                    {
                        db.Entry(tbl_Custlist).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ct.Commit();
                        Session["UserName"] = tbl_Custlist.Name;
                        Session["Email"] = tbl_Custlist.Email.ToString();
                        Session["PhoneNum"] = tbl_Custlist.Phone_Number.ToString();
                        Session["UserId"] = tbl_Custlist.usernamecust.ToString();
                        Session["Address"] = tbl_Custlist.Address.ToString();
                        string message = "Account Information Updated";
                        TempData["message"] = message;
                        return RedirectToAction("Index", "Home");
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    ct.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            string username = Session["UserId"].ToString();
            Tbl_Custacc custacc = db.Tbl_Custacc.Where(x => x.usernamecust == username).FirstOrDefault();
            return View(custacc);
        }

        [HttpPost]
        public ActionResult ChangePassword(string username, Tbl_Custacc custacc)
        {
            using (DbContextTransaction ct = db.Database.BeginTransaction())
            {
                try
                {
                    if (custacc != null)
                    {
                        username = Session["UserId"].ToString();
                        Tbl_Custacc custacc1 = db.Tbl_Custacc.FirstOrDefault(x => x.usernamecust.Equals(username));

                        custacc1.password = Crypto.Hash(custacc.password);
                        custacc1.confirm_pass = Crypto.Hash(custacc.confirm_pass);

                        db.Entry(custacc1).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ct.Commit();
                        string message = "Account Password Updated";
                        TempData["message"] = message;
                        return RedirectToAction("Index", "Home");
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    ct.Rollback();
                    ViewData["data"] = ex.Message;
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult UploadEvidence(int id)
        {
            Tbl_Order order = db.Tbl_Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        [HttpPost]
        public ActionResult UploadEvidence(Tbl_Order order, HttpPostedFileBase postedFile)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    if (Request.Files["postedFile"] != null)
                    {
                        byte[] Image;
                        using (var binaryReader = new BinaryReader(Request.Files["postedFile"].InputStream))
                        {
                            Image = binaryReader.ReadBytes(Request.Files["postedFile"].ContentLength);
                        }
                        var existingOrder = db.Tbl_Order.FirstOrDefault(s => s.IDorder == order.IDorder);
                        existingOrder.Transfer_Evidence = Image;
                        existingOrder.Bank_Name = order.Bank_Name;
                        existingOrder.Bank_Account_Number = order.Bank_Account_Number;
                        existingOrder.Bank_Customer_Name = order.Bank_Customer_Name;
                        db.SaveChanges();
                        ts.Commit();

                        SendNotificationEmail("Pay");
                        string message = "Success. Payment Updated";
                        TempData["message"] = message;
                        return RedirectToAction("History");
                    }
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }
    }
}