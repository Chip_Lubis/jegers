﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.IO;

namespace JegErs.Controllers
{
    public class SuperAdminController : Controller
    {
        JegErsContext db = new JegErsContext();
        // GET: SuperAdmin
        [HttpGet]
        public ActionResult SettingAccount()
        {
            string username = Session["UserId"].ToString();
            Tbl_SAdminacc sa = db.Tbl_SAdminacc.Where(x => x.usernamesadmin == username).FirstOrDefault();
            return View(sa);
        }

        [HttpPost]
        public ActionResult SettingAccount(string username, Tbl_SAdminacc sadminacc)
        {
            using (DbContextTransaction ct = db.Database.BeginTransaction())
            {
                try
                {
                    if (sadminacc != null)
                    {
                        username = Session["UserId"].ToString();
                        Tbl_SAdminacc sadminacc1 = db.Tbl_SAdminacc.FirstOrDefault(x => x.usernamesadmin.Equals(username));

                        sadminacc1.Name = sadminacc.Name;

                        db.Entry(sadminacc1).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ct.Commit();
                        Session["UserName"] = sadminacc1.Name;
                        string message = "Account Information Updated";
                        TempData["message"] = message;
                        return RedirectToAction("Index", "Home");
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    ct.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            string username = Session["UserId"].ToString();
            Tbl_SAdminacc sa = db.Tbl_SAdminacc.Where(x => x.usernamesadmin == username).FirstOrDefault();
            return View(sa);
        }

        [HttpPost]
        public ActionResult ChangePassword(string username, Tbl_SAdminacc sadminacc)
        {
            using (DbContextTransaction ct = db.Database.BeginTransaction())
            {
                try
                {
                    if (sadminacc != null)
                    {
                        username = Session["UserId"].ToString();
                        Tbl_SAdminacc sadminacc1 = db.Tbl_SAdminacc.FirstOrDefault(x => x.usernamesadmin.Equals(username));

                        sadminacc1.password = Crypto.Hash(sadminacc.password);

                        db.Entry(sadminacc1).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ct.Commit();
                        string message = "Account Password Updated";
                        TempData["message"] = message;
                        return RedirectToAction("Index", "Home");
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    ct.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }


        [HttpGet]
        public ActionResult EditTermsRules(int id)
        {
            Tbl_TermsAndRules rules = db.Tbl_TermsAndRules.Where(x => x.id_termsandrules == id).FirstOrDefault();
            return View(rules);
        }
        [HttpPost]
        public ActionResult EditTermsRules(Tbl_TermsAndRules rules, HttpPostedFileBase postedFile)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    if (Request.Files["postedFile"] != null)
                    {
                        byte[] Image;
                        using (var binaryReader = new BinaryReader(Request.Files["postedFile"].InputStream))
                        {
                            Image = binaryReader.ReadBytes(Request.Files["postedFile"].ContentLength);
                        }
                        rules.image_terms_rules = Image;
                        db.Entry(rules).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        string message = "Term Updated";
                        TempData["message"] = message;
                        return RedirectToAction("TermsRules");
                    }
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }


        public ActionResult DeleteTermsRules(int id)
        {
            Tbl_TermsAndRules rules = db.Tbl_TermsAndRules.Where(x => x.id_termsandrules == id).FirstOrDefault();
            return View(rules);
        }
        [HttpPost]
        public ActionResult DeleteTermsRules(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_TermsAndRules rules = db.Tbl_TermsAndRules.Where(x => x.id_termsandrules == id).FirstOrDefault();
                    db.Tbl_TermsAndRules.Remove(rules);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Term Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("TermsRules");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        [HttpGet]
        public ActionResult ChangeInformation()
        {
            Tbl_Jegers_Info info = db.Tbl_Jegers_Info.Where(x => x.id_jegers_info == 1).FirstOrDefault();
            return View(info);
        }
        [HttpPost]
        public ActionResult ChangeInformation(Tbl_Jegers_Info info)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(info).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Informations Updated";
                    TempData["message"] = message;
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult TermsRules()
        {
            ViewBag.Message = TempData["message"];
            return View(db.Tbl_TermsAndRules.OrderByDescending(x => x.id_termsandrules).ToList());
        }


        [HttpGet]
        public ActionResult CreateTermsRules()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateTermsRules(Tbl_TermsAndRules rules, HttpPostedFileBase postedFile)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                    {
                        bytes = br.ReadBytes(postedFile.ContentLength);
                    }
                    rules.image_terms_rules = bytes;
                    db.Tbl_TermsAndRules.Add(rules);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "New Term Created";
                    TempData["message"] = message;
                    return RedirectToAction("TermsRules");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
            }
            return View();
        }
        public ActionResult ItemReporting()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            string urlReportServer = "http://desktop-ati0749/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/JegErs_Item_List_Reporting";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }

        public ActionResult OrderReporting()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            string urlReportServer = "http://desktop-ati0749/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/JegErs_Order_List_Reporting";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult CustomerReporting()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            string urlReportServer = "http://desktop-ati0749/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/JegErs_Customer_List_Reporting";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult AdminPerformReporting()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            string urlReportServer = "http://desktop-ati0749/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/JegErs_Admin_Performance_Reporting";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult IncomeReporting()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            string urlReportServer = "http://desktop-ati0749/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/JegErs_Income_Reporting";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ExpenseReporting()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            string urlReportServer = "http://desktop-ati0749/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/JegErs_Expense_Reporting";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult CustomerList()
        {
            return View(db.Tbl_Custlist.ToList().OrderByDescending(a => a.IDcustlist));
        }

        public ActionResult AdminList()
        {
            ViewBag.Message = TempData["message"];
            return View(db.Tbl_Adminacc.ToList().OrderByDescending(a => a.id_account));
        }

        [HttpGet]
        public ActionResult AdminRegistration()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChangeKgPrice()
        {
            Tbl_KgPrice kg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();
            return View(kg);
        }

        [HttpPost]
        public ActionResult ChangeKgPrice(Tbl_KgPrice kg)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(kg).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Kg Price updated";
                    TempData["message"] = message;
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        [HttpPost]
        public ActionResult AdminRegistration(Tbl_Adminacc adminacc)
        {
            try
            {
                #region //Email or Username is already Exist 
                var isExist = IsEmailExist(adminacc.Email);
                var isExist2 = IsUsernameExist(adminacc.usernameadmin);
                if (isExist)
                {
                    ViewBag.Message = "Email Already Exist";
                    return View();
                }
                if (isExist2)
                {
                    ViewBag.Message = "Username Already Exist";
                    return View();
                }
                #endregion
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    adminacc.password = Crypto.Hash(adminacc.password);
                    try
                    {
                        db.Tbl_Adminacc.Add(adminacc);
                        db.SaveChanges();
                        ts.Commit();
                        string message = "New Admin Account Created";
                        TempData["message"] = message;
                        return RedirectToAction("AdminList", "SuperAdmin");
                    }
                    catch (Exception ex)
                    {
                        ts.Rollback();
                        ViewData["data"] = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewData["data"] = ex.Message;
            }
            return View();
        }

        [NonAction]
        public bool IsEmailExist(string emailID)
        {
            var v = db.Tbl_Custacc.Where(a => a.Email == emailID).FirstOrDefault();
            var v2 = db.Tbl_Adminacc.Where(a => a.Email == emailID).FirstOrDefault();
            var v3 = db.Tbl_SAdminacc.Where(a => a.Email == emailID).FirstOrDefault();
            return (v != null || v2 != null || v3 != null);
        }

        [NonAction]
        public bool IsUsernameExist(string userID)
        {
            var v = db.Tbl_Custacc.Where(a => a.usernamecust == userID).FirstOrDefault();
            var v2 = db.Tbl_Adminacc.Where(a => a.usernameadmin == userID).FirstOrDefault();
            var v3 = db.Tbl_SAdminacc.Where(a => a.usernamesadmin == userID).FirstOrDefault();
            return (v != null || v2 != null || v3 != null);
        }

        [HttpGet]
        public ActionResult AdminEdit(int id)
        {
            Tbl_Adminacc adminacc = db.Tbl_Adminacc.Find(id);
            return View(adminacc);
        }

        [HttpPost]
        public ActionResult AdminEdit(int id, Tbl_Adminacc adminacc)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Adminacc admin = db.Tbl_Adminacc.FirstOrDefault(x => x.id_account.Equals(id));

                    admin.usernameadmin = adminacc.usernameadmin;
                    admin.Email = adminacc.Email;
                    admin.Name = adminacc.Name;

                    db.Entry(admin).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Admin Account Updated";
                    TempData["message"] = message;
                    return RedirectToAction("AdminList");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult AdminDelete(int id)
        {
            Tbl_Adminacc adminacc = db.Tbl_Adminacc.Find(id);
            return View(adminacc);
        }

        [HttpPost]
        public ActionResult AdminDelete(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Adminacc adminacc = db.Tbl_Adminacc.Where(x => x.id_account == id).FirstOrDefault();
                    db.Tbl_Adminacc.Remove(adminacc);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Admin Account Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("AdminList", "SuperAdmin");
                }
                catch (DbEntityValidationException)
                {
                    ts.Rollback();
                    throw;
                }
            }
        }

        public ActionResult Expense()
        {
            ViewBag.Message = TempData["message"];
            return View(db.Tbl_Expense.ToList().OrderByDescending(a => a.id_Expense));
        }

        [HttpGet]
        public ActionResult CreateExpense()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateExpense(Tbl_Expense expense)
        {
            try
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Tbl_Expense.Add(expense);
                        db.SaveChanges();
                        ts.Commit();
                        string message = "New Expense Created";
                        TempData["message"] = message;
                        return RedirectToAction("Expense");
                    }
                    catch (Exception ex)
                    {
                        ts.Rollback();
                        ViewData["data"] = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewData["data"] = ex.Message;
            }
            return View();
        }


        public ActionResult EditExpense(int id)
        {
            Tbl_Expense expense = db.Tbl_Expense.Where(x => x.id_Expense == id).FirstOrDefault();
            return View(expense);
        }
        [HttpPost]
        public ActionResult EditExpense(Tbl_Expense expense)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(expense).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Expense Updated";
                    TempData["message"] = message;
                    return RedirectToAction("Expense");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }


        public ActionResult DeleteExpense(int id)
        {
            Tbl_Expense expense = db.Tbl_Expense.Where(x => x.id_Expense == id).FirstOrDefault();
            return View(expense);
        }
        [HttpPost]
        public ActionResult DeleteExpense(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Expense expense = db.Tbl_Expense.Where(x => x.id_Expense == id).FirstOrDefault();
                    db.Tbl_Expense.Remove(expense);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Expense Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("Expense");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }
    }
}