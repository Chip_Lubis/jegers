﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JegErs.Controllers
{
    public class HomeController : Controller
    {
        JegErsContext db = new JegErsContext();

        public ActionResult Index()
        {
            var dataBanner = from b in db.Tbl_Banner
                             orderby b.Name ascending
                             select b;

            int max = 0;
            foreach (var item in dataBanner)
            {
                if (item.Active_Banner == "Yes")
                {
                    if (max < 5)
                    {
                        max++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            String[] imageSrc = new String[max];
            int i = 0;
            int count = 0;
            foreach (var item in dataBanner)
            {
                if (item.Active_Banner == "Yes")
                {
                    if (count < 5)
                    {
                        string imageBase64 = Convert.ToBase64String(item.Data);
                        imageSrc[i] = string.Format("data:image/gif;base64,{0}", imageBase64);
                        i++;
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            ViewBag.ImageCollection = String.Join("\",\"", imageSrc);
            ViewBag.Message = TempData["message"];
            return View();
        }

        public ActionResult History()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SettingAccount()
        {
            string username = Session["UserId"].ToString();
            Tbl_Custlist tbl_Custlist = db.Tbl_Custlist.Where(x => x.usernamecust == username).FirstOrDefault();
            Tbl_Adminacc tbl_Adminacc = db.Tbl_Adminacc.Where(x => x.usernameadmin == username).FirstOrDefault();
            if(tbl_Custlist == null)
            {
                return View(tbl_Adminacc);
            }
            else
            {
                return View(tbl_Custlist);
            }
        }

        [HttpPost]
        public ActionResult SettingAccount(Tbl_Custlist tbl_Custlist, Tbl_Adminacc tbl_Adminacc)
        {
            using (DbContextTransaction ct = db.Database.BeginTransaction())
            {
                try
                {
                    if (tbl_Custlist != null)
                    {
                        db.Entry(tbl_Custlist).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ct.Commit();
                        Session["UserName"] = tbl_Custlist.Name.ToString();
                        //return RedirectToAction("Index");
                        return RedirectToAction("Order", "Customer");
                    }
                    else
                    {
                        db.Entry(tbl_Adminacc).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ct.Commit();
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    ct.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }
    }
}