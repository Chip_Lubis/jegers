﻿using RegistrationAndLogin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using JegErs.Models;
using System.Security.Principal;
using Microsoft.Reporting.Map.WebForms.BingMaps;

namespace JegErs.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude = "IsEmailVerified,Activation_Code")] CustomerModel account)
        {
            try
            {
                bool Status = false;
                string message = "";
                //
                // Model Validation 
                if (ModelState.IsValid)
                {

                    #region //Email is already Exist 
                    var isExist = IsEmailExist(account.ModelAcc.Email);
                    var isExist2 = IsUsernameExist(account.ModelAcc.usernamecust);
                    if (isExist)
                    {
                        ViewBag.Message = "Email Already Exist";
                        return View();
                    }
                    if (isExist2)
                    {
                        ViewBag.Message = "Username Already Exist";
                        return View();
                    }
                    #endregion

                    #region Generate Activation Code 
                    account.ModelAcc.Activation_Code = Guid.NewGuid();
                    #endregion

                    #region  Password Hashing 
                    account.ModelAcc.password = Crypto.Hash(account.ModelAcc.password);
                    account.ModelAcc.confirm_pass = Crypto.Hash(account.ModelAcc.confirm_pass); //
                    #endregion
                    account.ModelAcc.IsEmailVerified = false;

                    #region Save to Database
                    using (JegErsContext dc = new JegErsContext())
                    {
                        dc.Tbl_Custacc.Add(account.ModelAcc);
                        dc.SaveChanges();
                        Tbl_Custlist cust = new Tbl_Custlist()
                        {
                            Name = account.Name,
                            Address = account.Address,
                            Phone_Number = account.Phone_Number,
                            Email = account.ModelAcc.Email,
                            usernamecust = account.ModelAcc.usernamecust
                        };
                        dc.Tbl_Custlist.Add(cust);
                        dc.SaveChanges();

                        //Send Email to User
                        SendVerificationLinkEmail(account.ModelAcc.Email, account.ModelAcc.Activation_Code.ToString());
                        message = "Registration successfully done. Account activation link " +
                            " has been sent to your email id: " + account.ModelAcc.Email;
                        Status = true;
                    }
                    #endregion
                }
                else
                {
                    message = "Invalid Request";
                }

                ViewBag.Message = message;
                ViewBag.Status = Status;
                return View(account);
            }
            catch (Exception ex)
            {
                ViewData["data"] = ex.Message;
                return View();
            }
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool Status = false;
            using (JegErsContext dc = new JegErsContext())
            {
                dc.Configuration.ValidateOnSaveEnabled = false; // to avoid 
                                                                // Confirm password does not match issue on save changes
                var v = dc.Tbl_Custacc.Where(a => a.Activation_Code == new Guid(id)).FirstOrDefault();
                if (v != null)
                {
                    v.IsEmailVerified = true;
                    dc.SaveChanges();
                    Status = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request";
                }
            }
            ViewBag.Status = Status;
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        //Login POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin login, string ReturnUrl = "")
        {
            string message = "";
            using (JegErsContext dc = new JegErsContext())
            {
                var v = dc.Tbl_Custacc.Where(a => a.usernamecust == login.Email_username).FirstOrDefault();
                var v2 = dc.Tbl_Adminacc.Where(a => a.usernameadmin == login.Email_username).FirstOrDefault();
                var v3 = dc.Tbl_SAdminacc.Where(a => a.usernamesadmin == login.Email_username).FirstOrDefault();
                if (v == null && v2 == null && v3 == null)
                {
                    v = dc.Tbl_Custacc.Where(a => a.Email == login.Email_username).FirstOrDefault();
                    v2 = dc.Tbl_Adminacc.Where(a => a.Email == login.Email_username).FirstOrDefault();
                    v3 = dc.Tbl_SAdminacc.Where(a => a.Email == login.Email_username).FirstOrDefault();
                }

                if (v != null || v2 != null || v3 != null)
                {
                    if (v != null)
                    {
                        if (v.IsEmailVerified == false)
                        {
                            ViewBag.Message = "Please verify your email first";
                            return View();
                        }

                        if (string.Compare(Crypto.Hash(login.Password), v.password) == 0)
                        {
                            var nama = dc.Tbl_Custlist.Where(a => a.usernamecust == v.usernamecust).FirstOrDefault();
                            Session["UserName"] = nama.Name.ToString();
                            Session["Email"] = nama.Email.ToString();
                            Session["PhoneNum"] = nama.Phone_Number.ToString();
                            Session["UserId"] = nama.usernamecust.ToString();
                            Session["Address"] = nama.Address.ToString();
                            Session["CustId"] = nama.IDcustlist;
                            Session["Role"] = "Customer";

                            message = "Login Success. Welcome to JegErs";
                            TempData["message"] = message;

                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            message = "Wrong Username/Email or Password";
                        }

                    }
                    else if (v2 != null)
                    {
                        if (string.Compare(Crypto.Hash(login.Password), v2.password) == 0)
                        {
                            var nama = dc.Tbl_Adminacc.Where(a => a.usernameadmin == v2.usernameadmin).FirstOrDefault();
                            Session["UserName"] = nama.Name.ToString();
                            Session["UserId"] = nama.usernameadmin.ToString();
                            Session["Role"] = "Admin";

                            message = "Login Success. Welcome to JegErs";
                            TempData["message"] = message;

                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            message = "Wrong Username/Email or Password";
                        }
                    }
                    else if (v3 != null)
                    {
                        if (string.Compare(Crypto.Hash(login.Password), v3.password) == 0)
                        {
                            var nama = dc.Tbl_SAdminacc.Where(a => a.usernamesadmin == v3.usernamesadmin).FirstOrDefault();
                            Session["UserName"] = nama.Name.ToString();
                            Session["UserId"] = nama.usernamesadmin.ToString();
                            Session["Role"] = "SuperAdmin";

                            message = "Login Success. Welcome to JegErs";
                            TempData["message"] = message;

                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            message = "Wrong Username/Email or Password";
                        }
                    }
                }
                else
                {
                    message = "Wrong Username/Email or Password";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        public ActionResult Logout()
        {
            Session["Username"] = null;
            if (Session["Role"] != null)
            {
                Session["Role"] = null;
            }
            return RedirectToAction("Index", "Home");
        }

        [NonAction]
        public bool IsEmailExist(string emailID)
        {
            using (JegErsContext dc = new JegErsContext())
            {
                var v = dc.Tbl_Custacc.Where(a => a.Email == emailID).FirstOrDefault();
                return v != null;
            }
        }

        [NonAction]
        public bool IsUsernameExist(string userID)
        {
            using (JegErsContext dc = new JegErsContext())
            {
                var v = dc.Tbl_Custacc.Where(a => a.usernamecust == userID).FirstOrDefault();
                return v != null;
            }
        }

        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyUrl = "/Login/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("jegerslaundry@gmail.com", "JegErs Laundry");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "RajaLaundry1"; // Replace with actual password

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Your account is successfully created!";
                body = "<br/><br/>We are excited to tell you that your Dotnet Awesome account is" +
                    " successfully created. Please click on the below link to verify your account" +
                    " <br/><br/><a href='" + link + "'>" + link + "</a> ";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi,<br/><br/>We got request for reset your account password. Please click on the below link to reset your password" +
                    "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string EmailID)
        {
            //Verify Email ID
            //Generate Reset password link 
            //Send Email 
            string message = "";

            using (JegErsContext dc = new JegErsContext())
            {
                var account = dc.Tbl_Custacc.Where(a => a.Email == EmailID).FirstOrDefault();
                if (account != null)
                {
                    //Send email for reset password
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.Email, resetCode, "ResetPassword");
                    account.ResetPasswordCode = Guid.Parse(resetCode);
                    dc.Configuration.ValidateOnSaveEnabled = false;
                    dc.SaveChanges();
                    message = "Reset password link has been sent to your email id.";
                }
                else
                {
                    message = "Account not found";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        [HttpGet]
        public ActionResult ResetPassword(string id)
        {
            //Verify the reset password link
            //Find account associated with this link
            //redirect to reset password page
            if (string.IsNullOrWhiteSpace(id))
            {
                return HttpNotFound();
            }

            using (JegErsContext dc = new JegErsContext())
            {
                var user = dc.Tbl_Custacc.Where(a => a.ResetPasswordCode.ToString() == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using (JegErsContext dc = new JegErsContext())
                {
                    var user = dc.Tbl_Custacc.Where(a => a.ResetPasswordCode.ToString() == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        user.password = Crypto.Hash(model.NewPassword);
                        user.confirm_pass = Crypto.Hash(model.NewPassword);
                        user.ResetPasswordCode = Guid.Empty;
                        dc.Configuration.ValidateOnSaveEnabled = false;
                        dc.SaveChanges();
                        message = "New password updated successfully";
                    }
                }
            }
            else
            {
                message = "Something invalid";
            }
            ViewBag.Message = message;
            return View(model);
        }
    }

}