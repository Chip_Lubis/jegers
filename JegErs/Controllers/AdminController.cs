﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.IO;
using JegErs.Models;
using System.Net.Mail;
using System.Net;
using System.Data.Entity.Validation;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Web.Helpers;

namespace JegErs.Controllers
{
    public class AdminController : Controller
    {
        JegErsContext db = new JegErsContext();

        public ActionResult ElectronicReceipt(int id)
        {
            var order = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();

            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.Height = Unit.Pixel(1100);
            string urlReportServer = "http://desktop-ati0749/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/JegErs_Electronic_Receipt";
            report.ServerReport.Refresh();

            ReportParameter parameters = new ReportParameter("IDorder", id.ToString(), true);
            report.ServerReport.SetParameters(parameters);
            report.ServerReport.Refresh();

            ViewBag.ReportViewer = report;
            return View();
        }

        public JsonResult AjaxCustomer(Customer cust)
        {
            db.Configuration.ProxyCreationEnabled = false;
            String selectedCat = cust.Name;
            var dataCust = (from a in db.Tbl_Custlist
                            where a.Name == selectedCat 
                            select new
                            {
                               a.Address, a.Email, a.Phone_Number
                            }).FirstOrDefault();

            return Json(dataCust, JsonRequestBehavior.AllowGet);
        }

        public class Customer
        {
            public string Name
            {
                get;
                set;
            }
        }

        public JsonResult AjaxPostCall(Measures measure)
        {
            db.Configuration.ProxyCreationEnabled = false;
            String selectedCat = measure.Item_name;
            var dataItem = (from a in db.Tbl_Item
                            join b in db.Tbl_Measure on a.Id_Measure equals b.Id_Measure into table1
                            from b in table1.ToList().DefaultIfEmpty()
                            where a.Item_Name == selectedCat
                            select new
                            {
                                a.Id_Item,
                                b.Measure1
                            });

            return Json(dataItem, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AjaxPostEditCall(Measures measure)
        {
            db.Configuration.ProxyCreationEnabled = false;
            String selectedCat = measure.Item_name;
            var dataItem = (from a in db.Tbl_Item
                           join b in db.Tbl_Measure on a.Id_Measure equals b.Id_Measure into table1
                           from b in table1.ToList().DefaultIfEmpty()
                           where a.Item_Name == selectedCat
                            select new
                            {
                                a.Id_Item,
                                b.Measure1
                            });

            return Json(dataItem, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AjaxPopulateEdit(Measures measure)
        {
            db.Configuration.ProxyCreationEnabled = false;
            int selectedCat = measure.Id_Order;
            var dataTable = db.Tbl_Itemdetail.Where(x => x.IDorder == selectedCat).ToList().DefaultIfEmpty();
 
            var ItemList = from firstList in db.Tbl_Item
                           group firstList by firstList.Item_Name into newList
                           let m = newList.FirstOrDefault()
                           select new {
                               Item_Id = m.Id_Item,
                               Name_Item = m.Item_Name
                           };
  
            return Json(new
            {
                Table = dataTable,
                Item = ItemList
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AjaxPostEditCall2(Measures measure)
        {
            db.Configuration.ProxyCreationEnabled = false;
            int selectedCat = measure.Id_Item;
            var dataItem = db.Tbl_Item.Where(x => x.Id_Item == selectedCat).ToList().FirstOrDefault();
            //int selectedCat = Convert.ToInt32(measure.Id);
            //var dataItem = db.Tbl_Measure.Where(x => x.Id_Measure == selectedCat).ToList();
            return Json(dataItem, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AjaxPostCall2(Measures measure)
        {
            db.Configuration.ProxyCreationEnabled = false;
            int selectedCat = measure.Id_Item;
            var dataItem = db.Tbl_Item.Where(x => x.Id_Item == selectedCat).ToList().FirstOrDefault();

            //int selectedCat = Convert.ToInt32(measure.Id);
            //var dataItem = db.Tbl_Measure.Where(x => x.Id_Measure == selectedCat).ToList();
            return Json(dataItem, JsonRequestBehavior.AllowGet);
        }


        public class Measures
        {
            public string Item_name
            {
                get;
                set;
            }
            public int Id_Item
            {
                get;
                set;
            }

            public int Id_Order
            {
                get;
                set;
            }
        }

        public ActionResult repeat(OrderModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }
            return PartialView();
        }

        public ActionResult repeat2()
        {
            if (!ModelState.IsValid)
            {
                return PartialView();
            }
            return PartialView();
        }

        public ActionResult Item()
        {
            var itemmeasure = (from a in db.Tbl_Item
                               join b in db.Tbl_Measure
                               on a.Id_Measure equals b.Id_Measure into table1
                               from b in table1.ToList().DefaultIfEmpty()
                               orderby a.Id_Item descending
                               select new JoinItemMeasure
                               {
                                   ModelItem = a,
                                   ModelMeasure = b
                               });
            ViewBag.Message = TempData["message"];
            return View(itemmeasure.ToList().OrderByDescending(a => a.ModelItem.Id_Item));
        }

        public ActionResult Measure()
        {
            ViewBag.Message = TempData["message"];
            return View(db.Tbl_Measure.ToList().OrderByDescending(a => a.Id_Measure));
        }
        public ActionResult Rack()
        {
            ViewBag.Message = TempData["message"];
            return View(db.Tbl_Rack.ToList().OrderByDescending(a => a.Id_Rack));
        }
        public ActionResult Banner()
        {
            ViewBag.Message = TempData["message"];
            return View(db.Tbl_Banner.ToList().OrderByDescending(a => a.Id_Banner));
        }

        [HttpGet]
        public ActionResult CreateBanner()
        {
            return View("Banner");
        }

        [HttpPost]
        public ActionResult CreateBanner(HttpPostedFileBase postedFile)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                    {
                        bytes = br.ReadBytes(postedFile.ContentLength);
                    }
                    db.Tbl_Banner.Add(new Tbl_Banner
                    {
                        Name = Path.GetFileName(postedFile.FileName),
                        ContentType = postedFile.ContentType,
                        Data = bytes,
                        Active_Banner = "No"
                    });
                    db.SaveChanges();
                    ts.Commit();
                    string message = "New Banner Uploaded";
                    TempData["message"] = message;
                    return RedirectToAction("Banner");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View("Banner");
            }
        }

        [HttpGet]
        public ActionResult CreateItem()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateItem(Tbl_Item item)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    var isExist = IsItemExist(item.Item_Name, item.Id_Measure);
                    if (isExist)
                    {
                        ViewBag.Message = "Item Already Exist";
                        return View();
                    }

                    db.Tbl_Item.Add(item);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "New Item Created";
                    TempData["message"] = message;
                    return RedirectToAction("Item");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        [NonAction]
        public bool IsItemExist(string itemname, int? idmeasure)
        {
            using (JegErsContext dc = new JegErsContext())
            {
                var v = dc.Tbl_Item.Where(a => a.Item_Name == itemname && a.Id_Measure == idmeasure).FirstOrDefault();
                return v != null;
            }
        }

        [HttpGet]
        public ActionResult CreateRack()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateRack(Tbl_Rack rack)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    var isExist = IsRackExist(rack.Rack_location);
                    if (isExist)
                    {
                        ViewBag.Message = "Rack Already Exist";
                        return View();
                    }

                    db.Tbl_Rack.Add(rack);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "New Rack Created";
                    TempData["message"] = message;
                    return RedirectToAction("Rack");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }


        [NonAction]
        public bool IsRackExist(string rack)
        {
            using (JegErsContext dc = new JegErsContext())
            {
                var v = dc.Tbl_Rack.Where(a => a.Rack_location == rack).FirstOrDefault();
                return v != null;
            }
        }

        [HttpGet]
        public ActionResult CreateMeasure()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateMeasure(Tbl_Measure measure)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    var isExist = IsMeasureExist(measure.Measure1);
                    if (isExist)
                    {
                        ViewBag.Message = "Measure Already Exist";
                        return View();
                    }
                    db.Tbl_Measure.Add(measure);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "New Measure Created";
                    TempData["message"] = message;
                    return RedirectToAction("Measure");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }


        [NonAction]
        public bool IsMeasureExist(string measure)
        {
            using (JegErsContext dc = new JegErsContext())
            {
                var v = dc.Tbl_Measure.Where(a => a.Measure1 == measure).FirstOrDefault();
                return v != null;
            }
        }

        [HttpGet]
        public ActionResult EditBanner(int id)
        {
            Tbl_Banner banner = db.Tbl_Banner.Where(x => x.Id_Banner == id).FirstOrDefault();
            return View(banner);
        }

        [HttpPost]
        public ActionResult EditBanner(Tbl_Banner banner, HttpPostedFileBase postedFile)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    if (Request.Files["postedFile"] != null)
                    {
                        byte[] Image;
                        using (var binaryReader = new BinaryReader(Request.Files["postedFile"].InputStream))
                        {
                            Image = binaryReader.ReadBytes(Request.Files["postedFile"].ContentLength);
                        }
                        banner.Name = Path.GetFileName(Request.Files["postedFile"].FileName);
                        banner.ContentType = Request.Files["postedFile"].ContentType;
                        banner.Data = Image;

                        db.Entry(banner).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        string message = "Banner Updated";
                        TempData["message"] = message;
                        return RedirectToAction("Banner");
                    }
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult EditItem(int id)
        {
            Tbl_Item item = db.Tbl_Item.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            ViewBag.Id_Measure = new SelectList(db.Tbl_Measure, "Id_Measure", "Measure1", item.Id_Measure);
            return View(item);
        }

        [HttpPost]
        public ActionResult EditItem(Tbl_Item item)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Item Updated";
                    TempData["message"] = message;
                    return RedirectToAction("Item");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }
        public ActionResult EditRack(int id)
        {
            Tbl_Rack rack = db.Tbl_Rack.Where(x => x.Id_Rack == id).FirstOrDefault();
            return View(rack);
        }

        [HttpPost]
        public ActionResult EditRack(Tbl_Rack rack)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(rack).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Rack Updated";
                    TempData["message"] = message;
                    return RedirectToAction("Rack");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult EditMeasure(int id)
        {
            Tbl_Measure msr = db.Tbl_Measure.Where(x => x.Id_Measure == id).FirstOrDefault();
            return View(msr);
        }

        [HttpPost]
        public ActionResult EditMeasure(Tbl_Measure measure)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(measure).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Measure Updated";
                    TempData["message"] = message;
                    return RedirectToAction("Measure");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult EditOnthespot(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();
            var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

            EditOrderModel order = new EditOrderModel
            {
                IDorder = dataOrder.IDorder,
                IDitemdetail = dataOrderdetail.IDitemdetail,
                Customer_Name = dataOrder.Customer_Name,
                Email = dataOrder.Email,
                Phone_Number = dataOrder.Phone_Number,
                Delivery = dataOrder.Delivery,
                Status = dataOrder.Status,
                Paid_Already = dataOrder.Paid_Already,
                Id_Rack = dataOrder.Id_Rack,
                Address = dataOrder.Address,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Weight_KG = Convert.ToInt32(dataOrderdetail.Kg_Price) / Convert.ToInt32(hargakg.Per_Kg_Price),
                ObjItem = dataItemdetail
            };
            if(order.ObjItem.Contains(null))
            {
                ViewBag.Counter = 0;
            }
            else
            {
                ViewBag.Counter = order.ObjItem.Count();
            }
            return View(order);
        }

        [HttpPost]
        public ActionResult EditOnthespot(EditOrderModel model)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    var itemFile = model.ObjItem;

                    var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

                    int kg_price = model.Weight_KG * Convert.ToInt32(hargakg.Per_Kg_Price);
                    int? totspesial = 0;

                    if (model.Delivery == "No")
                    {
                        model.Delivery_Price = 0;
                    }

                    if (itemFile != null)
                    {
                        foreach (var item in itemFile)
                        {
                            if (item.Measure == null || item.Id_Item == null || item.Amount == null || item.Servicetype == null)
                            {
                                ViewBag.Message = "Please input special item detail correctly";
                                return View();
                            }
                            var dataItem = db.Tbl_Item.Where(x => x.Id_Item == item.Id_Item).FirstOrDefault();
                            if (item.Servicetype == "Laundry")
                            {
                                totspesial += item.Amount * dataItem.Laundry_Price;
                            }
                            else if (item.Servicetype == "DryClean")
                            {
                                totspesial += item.Amount * dataItem.DryClean_Price;
                            }
                        }
                    }

                    var existingOrder = db.Tbl_Order.FirstOrDefault(s => s.IDorder == model.IDorder);
                    existingOrder.Customer_Name = model.Customer_Name;
                    existingOrder.Email = model.Email;
                    existingOrder.Phone_Number = model.Phone_Number;
                    existingOrder.Delivery = model.Delivery;
                    existingOrder.Status = model.Status;
                    existingOrder.Paid_Already = model.Paid_Already;
                    existingOrder.Total_Price = model.Delivery_Price + kg_price + totspesial;
                    existingOrder.Id_Rack = model.Id_Rack;
                    existingOrder.Admin_Name = Session["Username"].ToString();
                    if (model.Delivery == "No")
                    {
                        existingOrder.Address = "";
                    }
                    else
                    {
                        existingOrder.Address = model.Address;
                    }
                    db.SaveChanges();

                    var existingOdetail = db.Tbl_Orderdetail.FirstOrDefault(s => s.IDorder == model.IDorder);
                    existingOdetail.Kg_Price = kg_price;
                    existingOdetail.Total_Special_Price = totspesial;
                    existingOdetail.Delivery_Price = model.Delivery_Price;
                    db.SaveChanges();

                    //var existingIdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == model.IDorder).ToList().DefaultIfEmpty();

                    Tbl_Itemdetail idetail = new Tbl_Itemdetail();

                    if (itemFile != null)
                    {
                        IEnumerable<Tbl_Itemdetail> deleteidetail = db.Tbl_Itemdetail.Where(x => x.IDorder == existingOrder.IDorder).ToList();
                        db.Tbl_Itemdetail.RemoveRange(deleteidetail);
                        db.SaveChanges();

                        foreach (var item in itemFile)
                        {
                            var dataItem = (from a in db.Tbl_Item
                                            join b in db.Tbl_Measure on a.Id_Measure equals b.Id_Measure into table1
                                            from b in table1.ToList().DefaultIfEmpty()
                                            where a.Id_Item == item.Id_Item
                                            select new
                                            {
                                                a.Item_Name,
                                                b.Measure1
                                            }).FirstOrDefault();

                            item.IDorder = existingOrder.IDorder;
                            item.IDitemdetail = existingOdetail.IDitemdetail;
                            item.Item_Name = dataItem.Item_Name;
                            item.Measure = dataItem.Measure1;
                            db.Tbl_Itemdetail.Add(item);
                        }
                        db.SaveChanges();
                    }
                    ts.Commit();
                    string message = "Order Updated";
                    TempData["message"] = message;
                    return RedirectToAction("Onthespot");
                }
                catch (DbEntityValidationException)
                {
                    ts.Rollback();
                    throw;
                }

            }
        }

        public ActionResult EditOnline(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();

            var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();


            EditOrderModel order = new EditOrderModel
            {
                IDorder = dataOrder.IDorder,
                IDitemdetail = dataOrderdetail.IDitemdetail,
                Customer_Name = dataOrder.Customer_Name,
                Email = dataOrder.Email,
                Phone_Number = dataOrder.Phone_Number,
                Delivery = dataOrder.Delivery,
                Status = dataOrder.Status,
                Paid_Already = dataOrder.Paid_Already,
                Id_Rack = dataOrder.Id_Rack,
                Address = dataOrder.Address,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Weight_KG = Convert.ToInt32(dataOrderdetail.Kg_Price) / Convert.ToInt32(hargakg.Per_Kg_Price),
                ObjItem = dataItemdetail
            };
            if (order.ObjItem.Contains(null))
            {
                ViewBag.Counter = 0;
            }
            else
            {
                ViewBag.Counter = order.ObjItem.Count();
            }
            return View(order);
        }

        [HttpPost]
        public ActionResult EditOnline(EditOrderModel model)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    var itemFile = model.ObjItem;

                    var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

                    int kg_price = model.Weight_KG * Convert.ToInt32(hargakg.Per_Kg_Price);
                    int? totspesial = 0;

                    if (model.Delivery == "No")
                    {
                        model.Delivery_Price = 0;
                    }

                    if (itemFile != null)
                    {
                        foreach (var item in itemFile)
                        {
                            if (item.Measure == null || item.Id_Item == null || item.Amount == null || item.Servicetype == null)
                            {
                                ViewBag.Message = "Please input special item detail correctly";
                                return View();
                            }
                            var dataItem = db.Tbl_Item.Where(x => x.Id_Item == item.Id_Item).FirstOrDefault();
                            if (item.Servicetype == "Laundry")
                            {
                                totspesial += item.Amount * dataItem.Laundry_Price;
                            }
                            else if (item.Servicetype == "DryClean")
                            {
                                totspesial += item.Amount * dataItem.DryClean_Price;
                            }
                        }
                    }

                    var existingOrder = db.Tbl_Order.FirstOrDefault(s => s.IDorder == model.IDorder);
                    existingOrder.Customer_Name = model.Customer_Name;
                    existingOrder.Email = model.Email;
                    existingOrder.Phone_Number = model.Phone_Number;
                    existingOrder.Delivery = model.Delivery;
                    existingOrder.Status = model.Status;
                    existingOrder.Paid_Already = model.Paid_Already;
                    existingOrder.Total_Price = model.Delivery_Price + kg_price + totspesial;
                    existingOrder.Id_Rack = model.Id_Rack;
                    existingOrder.Admin_Name = Session["Username"].ToString();
                    if (model.Delivery == "No")
                    {
                        existingOrder.Address = "";
                    }
                    else
                    {
                        existingOrder.Address = model.Address;
                    }
                    db.SaveChanges();

                    var existingOdetail = db.Tbl_Orderdetail.FirstOrDefault(s => s.IDorder == model.IDorder);
                    existingOdetail.Kg_Price = kg_price;
                    existingOdetail.Total_Special_Price = totspesial;
                    existingOdetail.Delivery_Price = model.Delivery_Price;
                    db.SaveChanges();

                    //var existingIdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == model.IDorder).ToList().DefaultIfEmpty();

                    Tbl_Itemdetail idetail = new Tbl_Itemdetail();

                    if (itemFile != null)
                    {
                        IEnumerable<Tbl_Itemdetail> deleteidetail = db.Tbl_Itemdetail.Where(x => x.IDorder == existingOrder.IDorder).ToList();
                        db.Tbl_Itemdetail.RemoveRange(deleteidetail);
                        db.SaveChanges();

                        foreach (var item in itemFile)
                        {
                            var dataItem = (from a in db.Tbl_Item
                                            join b in db.Tbl_Measure on a.Id_Measure equals b.Id_Measure into table1
                                            from b in table1.ToList().DefaultIfEmpty()
                                            where a.Id_Item == item.Id_Item
                                            select new
                                            {
                                                a.Item_Name,
                                                b.Measure1
                                            }).FirstOrDefault();

                            item.IDorder = existingOrder.IDorder;
                            item.IDitemdetail = existingOdetail.IDitemdetail;
                            item.Item_Name = dataItem.Item_Name;
                            item.Measure = dataItem.Measure1;
                            db.Tbl_Itemdetail.Add(item);
                        }
                        db.SaveChanges();
                    }
                    ts.Commit();

                    if (existingOrder.Status == "Finished")
                    {
                        SendNotificationEmail(existingOrder.Status, existingOrder.Email, existingOrder.Delivery, existingOrder.IDorder);
                        string message1 = "Order Online Updated";
                        TempData["message"] = message1;
                        return RedirectToAction("Online");
                    }

                    if (existingOrder.Total_Price != 0 && existingOrder.Total_Price != null && existingOrder.Status == "Inprocess")
                    {
                        SendNotificationEmail("TotalHarga", existingOrder.Email, "", existingOrder.IDorder);
                        string message2 = "Order Online Updated";
                        TempData["message"] = message2;
                        return RedirectToAction("Online");
                    }

                    string message = "Order Online Updated";
                    TempData["message"] = message;
                    return RedirectToAction("Online");
                }
                catch (DbEntityValidationException)
                {
                    ts.Rollback();
                    throw;
                }

            }
        }

        public ActionResult DetailsOnthespot(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();
            var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

            EditOrderModel order = new EditOrderModel
            {
                Customer_Name = dataOrder.Customer_Name,
                Delivery = dataOrder.Delivery,
                Phone_Number = dataOrder.Phone_Number,
                Email = dataOrder.Email,
                Status = dataOrder.Status,
                Paid_Already = dataOrder.Paid_Already,
                Id_Rack = dataOrder.Id_Rack,
                Address = dataOrder.Address,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Weight_KG = Convert.ToInt32(dataOrderdetail.Kg_Price) / Convert.ToInt32(hargakg.Per_Kg_Price),
                Kg_Price = dataOrderdetail.Kg_Price,
                Total_Special_Price = dataOrderdetail.Total_Special_Price,
                ObjItem = dataItemdetail
            };

            return View(order);
        }

        public ActionResult DetailsOrder(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();
            var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

            EditOrderModel order = new EditOrderModel
            {
                Customer_Name = dataOrder.Customer_Name,
                Delivery = dataOrder.Delivery,
                Phone_Number = dataOrder.Phone_Number,
                Email = dataOrder.Email,
                Status = dataOrder.Status,
                Paid_Already = dataOrder.Paid_Already,
                Id_Rack = dataOrder.Id_Rack,
                Address = dataOrder.Address,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Weight_KG = Convert.ToInt32(dataOrderdetail.Kg_Price) / Convert.ToInt32(hargakg.Per_Kg_Price),
                Kg_Price = dataOrderdetail.Kg_Price,
                Total_Special_Price = dataOrderdetail.Total_Special_Price,
                ObjItem = dataItemdetail
            };

            return View(order);
        }

        public ActionResult DetailsOnline(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();
            var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

            EditOrderModel order = new EditOrderModel
            {
                Customer_Name = dataOrder.Customer_Name,
                Delivery = dataOrder.Delivery,
                Phone_Number = dataOrder.Phone_Number,
                Email = dataOrder.Email,
                Status = dataOrder.Status,
                Paid_Already = dataOrder.Paid_Already,
                Id_Rack = dataOrder.Id_Rack,
                Address = dataOrder.Address,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Weight_KG = Convert.ToInt32(dataOrderdetail.Kg_Price) / Convert.ToInt32(hargakg.Per_Kg_Price),
                Kg_Price = dataOrderdetail.Kg_Price,
                Total_Special_Price = dataOrderdetail.Total_Special_Price,
                ObjItem = dataItemdetail
            };

            return View(order);
        }

        public ActionResult ChangeRackOnthespot(int id,string rack)
        {
            Tbl_Order order = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Rack rackname = db.Tbl_Rack.Where(x => x.Rack_location == ""+rack).FirstOrDefault();

                    order.Id_Rack = rackname.Id_Rack;
                    db.Entry(order).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Onthespot");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult ChangeRackOnline(int id, string rack)
        {
            Tbl_Order order = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Rack rackname = db.Tbl_Rack.Where(x => x.Rack_location == "" + rack).FirstOrDefault();

                    order.Id_Rack = rackname.Id_Rack;
                    db.Entry(order).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Online");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult FinishOrderOnthespot(int id)
        {
            Tbl_Order order = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    if (order.Paid_Already == "No")
                    {
                        Session["Message"] = "Order is not Paid yet";
                        return RedirectToAction("Onthespot");
                    }
                    if (order.Status == "Inprocess")
                    {
                        Session["Message"] = "Order is not Finished yet";
                        return RedirectToAction("Onthespot");
                    }

                    order.Order_finish = DateTime.Now;
                    db.Entry(order).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Onthespot");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult FinishOrderOnline(int id)
        {
            Tbl_Order order = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    if (order.Paid_Already == "No")
                    {
                        Session["Message"] = "Order is not Paid yet";
                        return RedirectToAction("Online");
                    }
                    if (order.Status == "Inprocess")
                    {
                        Session["Message"] = "Order is not Finished yet";
                        return RedirectToAction("Online");
                    }

                    order.Order_finish = DateTime.Now;
                    db.Entry(order).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Online");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult DeleteOrder(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();
            var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

            EditOrderModel order = new EditOrderModel
            {
                Customer_Name = dataOrder.Customer_Name,
                Email = dataOrder.Email,
                Phone_Number = dataOrder.Phone_Number,
                Delivery = dataOrder.Delivery,
                Status = dataOrder.Status,
                Paid_Already = dataOrder.Paid_Already,
                Id_Rack = dataOrder.Id_Rack,
                Address = dataOrder.Address,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Kg_Price = dataOrderdetail.Kg_Price,
                Total_Special_Price = dataOrderdetail.Total_Special_Price,
                Weight_KG = Convert.ToInt32(dataOrderdetail.Kg_Price) / Convert.ToInt32(hargakg.Per_Kg_Price),
                ObjItem = dataItemdetail
            };

            return View(order);
        }

        [HttpPost]
        public ActionResult DeleteOrder(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    IEnumerable<Tbl_Itemdetail> idetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).ToList();
                    db.Tbl_Itemdetail.RemoveRange(idetail);
                    db.SaveChanges();

                    Tbl_Orderdetail odetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
                    db.Tbl_Orderdetail.Remove(odetail);
                    db.SaveChanges();

                    Tbl_Order order = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
                    db.Tbl_Order.Remove(order);
                    db.SaveChanges();

                    ts.Commit();
                    string message = "Order Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("Order");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult DeleteOnthespot(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();
            var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

            EditOrderModel order = new EditOrderModel
            {
                Customer_Name = dataOrder.Customer_Name,
                Email = dataOrder.Email,
                Phone_Number = dataOrder.Phone_Number,
                Delivery = dataOrder.Delivery,
                Status = dataOrder.Status,
                Paid_Already = dataOrder.Paid_Already,
                Id_Rack = dataOrder.Id_Rack,
                Address = dataOrder.Address,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Kg_Price = dataOrderdetail.Kg_Price,
                Total_Special_Price = dataOrderdetail.Total_Special_Price,
                Weight_KG = Convert.ToInt32(dataOrderdetail.Kg_Price) / Convert.ToInt32(hargakg.Per_Kg_Price),
                ObjItem = dataItemdetail
            };

            return View(order);
        }

        [HttpPost]
        public ActionResult DeleteOnthespot(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    IEnumerable<Tbl_Itemdetail> idetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).ToList();
                    db.Tbl_Itemdetail.RemoveRange(idetail);
                    db.SaveChanges();

                    Tbl_Orderdetail odetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
                    db.Tbl_Orderdetail.Remove(odetail);
                    db.SaveChanges();

                    Tbl_Order order = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
                    db.Tbl_Order.Remove(order);
                    db.SaveChanges();

                    ts.Commit();
                    string message = "Order Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("Onthespot");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult DeleteOnline(int id)
        {
            var dataOrder = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
            var dataOrderdetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
            List<Tbl_Itemdetail> dataItemdetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).DefaultIfEmpty().ToList();
            var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

            EditOrderModel order = new EditOrderModel
            {
                Customer_Name = dataOrder.Customer_Name,
                Email = dataOrder.Email,
                Phone_Number = dataOrder.Phone_Number,
                Delivery = dataOrder.Delivery,
                Status = dataOrder.Status,
                Paid_Already = dataOrder.Paid_Already,
                Id_Rack = dataOrder.Id_Rack,
                Address = dataOrder.Address,
                Delivery_Price = dataOrderdetail.Delivery_Price,
                Kg_Price = dataOrderdetail.Kg_Price,
                Total_Special_Price = dataOrderdetail.Total_Special_Price,
                Weight_KG = Convert.ToInt32(dataOrderdetail.Kg_Price) / Convert.ToInt32(hargakg.Per_Kg_Price),
                ObjItem = dataItemdetail
            };

            return View(order);
        }

        [HttpPost]
        public ActionResult DeleteOnline(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    IEnumerable<Tbl_Itemdetail> idetail = db.Tbl_Itemdetail.Where(x => x.IDorder == id).ToList();
                    db.Tbl_Itemdetail.RemoveRange(idetail);
                    db.SaveChanges();

                    Tbl_Orderdetail odetail = db.Tbl_Orderdetail.Where(x => x.IDorder == id).FirstOrDefault();
                    db.Tbl_Orderdetail.Remove(odetail);
                    db.SaveChanges();

                    Tbl_Order order = db.Tbl_Order.Where(x => x.IDorder == id).FirstOrDefault();
                    db.Tbl_Order.Remove(order);
                    db.SaveChanges();

                    ts.Commit();
                    string message = "Order Online Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("Online");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }


        public ActionResult DeleteBanner(int id)
        {
            Tbl_Banner banner = db.Tbl_Banner.Where(x => x.Id_Banner == id).FirstOrDefault();
            return View(banner);
        }

        [HttpPost]
        public ActionResult DeleteBanner(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Banner banner = db.Tbl_Banner.Where(x => x.Id_Banner == id).FirstOrDefault();
                    db.Tbl_Banner.Remove(banner);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Banner Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("Banner");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult DeleteItem(int id)
        {
            var itemmeasure = (from a in db.Tbl_Item
                               join b in db.Tbl_Measure
                               on a.Id_Measure equals b.Id_Measure into table1
                               where a.Id_Item == id
                               from b in table1.ToList()
                               select new JoinItemMeasure
                               {
                                   ModelItem = a,
                                   ModelMeasure = b
                               });
            return View(itemmeasure);
        }

        [HttpPost]
        public ActionResult DeleteItem(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Item item = db.Tbl_Item.Where(x => x.Id_Item == id).FirstOrDefault();
                    db.Tbl_Item.Remove(item);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Item Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("Item");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult DeleteRack(int id)
        {
            Tbl_Rack rack = db.Tbl_Rack.Where(x => x.Id_Rack == id).FirstOrDefault();
            return View(rack);
        }

        [HttpPost]
        public ActionResult DeleteRack(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Rack rack = db.Tbl_Rack.Where(x => x.Id_Rack == id).FirstOrDefault();
                    db.Tbl_Rack.Remove(rack);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Rack Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("Rack");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult DeleteMeasure(int id)
        {
            Tbl_Measure msr = db.Tbl_Measure.Where(x => x.Id_Measure == id).FirstOrDefault();
            return View(msr);
        }

        [HttpPost]
        public ActionResult DeleteMeasure(int id, FormCollection collection)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Tbl_Measure measure = db.Tbl_Measure.Where(x => x.Id_Measure == id).FirstOrDefault();
                    db.Tbl_Measure.Remove(measure);
                    db.SaveChanges();
                    ts.Commit();
                    string message = "Measure Deleted";
                    TempData["message"] = message;
                    return RedirectToAction("Measure");
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult Onthespot()
        {
            var order = (from a in db.Tbl_Order
                         join b in db.Tbl_Rack on a.Id_Rack equals b.Id_Rack into table1
                         from b in table1.ToList().DefaultIfEmpty()
                         where a.Location == "Onthespot" && a.Order_finish == null
                         orderby a.IDorder descending
                         select new JoinOrderRack
                         {
                             ModelOrder = a,
                             ModelRack = b
                         });
            ViewBag.Message = TempData["message"];
            return View(order);
        }
        public ActionResult Order()
        {
            var order = (from a in db.Tbl_Order
                         join b in db.Tbl_Rack on a.Id_Rack equals b.Id_Rack into table1
                         from b in table1.ToList().DefaultIfEmpty()
                         where a.Order_finish.HasValue
                         orderby a.IDorder descending
                         select new JoinOrderRack
                         {
                             ModelOrder = a,
                             ModelRack = b
                         });
            ViewBag.Message = TempData["message"];
            return View(order);
        }

        public ActionResult Online()
        {
            var order = (from a in db.Tbl_Order
                         join b in db.Tbl_Rack on a.Id_Rack equals b.Id_Rack into table1
                         from b in table1.ToList().DefaultIfEmpty()
                         where a.Location == "Online" && a.Order_finish == null
                         orderby a.IDorder descending
                         select new JoinOrderRack
                         {
                             ModelOrder = a,
                             ModelRack = b
                         });
            ViewBag.Message = TempData["message"];
            return View(order);
        }

        [HttpGet]
        public ActionResult CreateOnthespot()
        {
            var data = (from a in db.Tbl_Custlist
                        select new
                        {
                            a.Name
                        }).ToList();
            int counter = 0;
            var json = "[";
            foreach (var item in data)
            {
                json += "\"" + item.Name + "\"";
                if (counter < data.Count-1)
                {
                    json += ",";
                }
                counter++;
            }
            json += "]";
            ViewBag.Customer = json;

            return View();
        }

        public class AutocompleteMapping
        {
            public string CustomerName { get; set; }
            public string Address { get; set; }
            public string Email { get; set; }
            public string PhoneNumber { get; set; }
        }

        [HttpPost]
        public ActionResult CreateOnthespot(OrderModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    if (model.ModelOrder.Customer_Name != null && model.Weight_KG != null && model.ModelOrder.Delivery != null && model.ModelOrder.Paid_Already != null)
                    {
                        var itemFile = model.ObjItem;

                        if (model.ModelOrder.Delivery == "No")
                        {
                            model.Delivery_Price = 0;
                        }

                        var hargakg = db.Tbl_KgPrice.Where(x => x.id_kg_price == 1).FirstOrDefault();

                        int? totalhargakg = model.Weight_KG * hargakg.Per_Kg_Price;

                        int? totspesial = 0;

                        if (itemFile != null)
                        {
                            foreach (var item in itemFile)
                            {
                                if (item.Measure == null || item.Id_Item == null || item.Amount == null || item.Servicetype == null)
                                {
                                    ViewBag.Message = "Please input special item detail correctly";
                                    return View();
                                }
                                var dataItem = db.Tbl_Item.Where(x => x.Id_Item == item.Id_Item).FirstOrDefault();
                                if (item.Servicetype == "Laundry")
                                {
                                    totspesial += item.Amount * dataItem.Laundry_Price;
                                }
                                else if (item.Servicetype == "DryClean")
                                {
                                    totspesial += item.Amount * dataItem.DryClean_Price;
                                }
                            }
                        }

                        string rack = Convert.ToString(model.ModelOrder.Customer_Name[0]);
                        var idrack = db.Tbl_Rack.Where(x => x.Rack_location == rack).FirstOrDefault();

                        Tbl_Order order = new Tbl_Order()
                        {
                            Customer_Name = model.ModelOrder.Customer_Name,
                            Email = model.ModelOrder.Email,
                            Phone_Number = model.ModelOrder.Phone_Number,
                            Delivery = model.ModelOrder.Delivery,
                            Status = "Inprocess",
                            Paid_Already = model.ModelOrder.Paid_Already,
                            Order_date = DateTime.Now,
                            Total_Price = model.Delivery_Price + totalhargakg + totspesial,
                            Location = "Onthespot",
                            Address = model.Address,
                            Id_Rack = Convert.ToInt32(idrack.Id_Rack),
                            Admin_Name = Session["Username"].ToString()
                        };
                        db.Tbl_Order.Add(order);
                        db.SaveChanges();

                        var idorder = order.IDorder;
                        Tbl_Orderdetail odetail = new Tbl_Orderdetail()
                        {
                            IDorder = idorder,
                            Kg_Price = totalhargakg,
                            Total_Special_Price = totspesial,
                            Delivery_Price = model.Delivery_Price
                        };
                        db.Tbl_Orderdetail.Add(odetail);
                        db.SaveChanges();

                        var cust = (from a in db.Tbl_Custlist
                                    where a.Name == model.ModelOrder.Customer_Name
                                    select a).FirstOrDefault();
                        if(cust == null)
                        {
                            Tbl_Custlist custlist = new Tbl_Custlist()
                            {
                                Name = order.Customer_Name,
                                Address = order.Address,
                                Phone_Number = order.Phone_Number,
                                Email = order.Email
                            };
                            db.Tbl_Custlist.Add(custlist);
                            db.SaveChanges();
                        }
                        else
                        {
                            var custlist = db.Tbl_Custlist.FirstOrDefault(s => s.Name == order.Customer_Name);
                            custlist.Address = order.Address;
                            custlist.Phone_Number = order.Phone_Number;
                            custlist.Email = order.Email;
                            db.SaveChanges();
                        }

                        var idorderdetail = odetail.IDitemdetail;
                        Tbl_Itemdetail idetail = new Tbl_Itemdetail();

                        if (itemFile != null)
                        {
                            foreach (var item in itemFile)
                            {
                                var dataItem = (from a in db.Tbl_Item
                                                join b in db.Tbl_Measure on a.Id_Measure equals b.Id_Measure into table1
                                                from b in table1.ToList().DefaultIfEmpty()
                                                where a.Id_Item == item.Id_Item
                                                select new
                                                {
                                                    a.Item_Name,
                                                    b.Measure1
                                                }).FirstOrDefault();

                                item.IDorder = idorder;
                                item.IDitemdetail = idorderdetail;
                                item.Item_Name = dataItem.Item_Name;
                                item.Measure = dataItem.Measure1;
                                db.Tbl_Itemdetail.Add(item);
                            }
                        }
                        db.SaveChanges();
                        dbTran.Commit();
                        string message = "New Order Onthespot Created";
                        TempData["message"] = message;
                        return RedirectToAction("Onthespot");
                    }
                    return View(); 
                }
                catch (DbEntityValidationException ex)
                {
                    ViewBag.Message = ex.Message;
                    dbTran.Rollback();
                    throw;
                }
                /*catch (Exception ex)
                {

                    return View();
                }*/
            }
        }

        [HttpGet]
        public ActionResult SettingAccount()
        {
            string username = Session["UserId"].ToString();
            Tbl_Adminacc tbl_Adminacc = db.Tbl_Adminacc.Where(x => x.usernameadmin == username).FirstOrDefault();
            return View(tbl_Adminacc);
        }

        [HttpPost]
        public ActionResult SettingAccount(string username, Tbl_Adminacc adminacc)
        {
            using (DbContextTransaction ct = db.Database.BeginTransaction())
            {
                try
                {
                    if (adminacc != null)
                    {
                        username = Session["UserId"].ToString();
                        Tbl_Adminacc adminacc1 = db.Tbl_Adminacc.FirstOrDefault(x => x.usernameadmin.Equals(username));

                        adminacc1.Name = adminacc.Name;

                        db.Entry(adminacc1).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ct.Commit();
                        Session["UserName"] = adminacc1.Name;
                        string message = "Account Information Updated";
                        TempData["message"] = message;
                        return RedirectToAction("Index", "Home");
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    ct.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            string username = Session["UserId"].ToString();
            Tbl_Adminacc tbl_Adminacc = db.Tbl_Adminacc.Where(x => x.usernameadmin == username).FirstOrDefault();
            return View(tbl_Adminacc);
        }

  [HttpPost]
        public ActionResult ChangePassword(string username, Tbl_Adminacc adminacc)
        {
            using (DbContextTransaction ct = db.Database.BeginTransaction())
            {
                try
                {
                    if (adminacc != null)
                    {
                        username = Session["UserId"].ToString();
                        Tbl_Adminacc adminacc1 = db.Tbl_Adminacc.FirstOrDefault(x => x.usernameadmin.Equals(username));

                        adminacc1.password = Crypto.Hash(adminacc.password);

                        db.Entry(adminacc1).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        ct.Commit();
                        string message = "Account Password Updated";
                        TempData["message"] = message;
                        return RedirectToAction("Index", "Home");
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    ct.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }


        [NonAction]
        public void SendNotificationEmail(string Notification, string emailcust, string delivery, int idorder)
        {
            var fromEmail = new MailAddress("jegerslaundry@gmail.com", "JegErs Laundry");
            var toEmail = new MailAddress(emailcust);
            var fromEmailPassword = "RajaLaundry1"; // Replace with actual password

            string subject = "";
            string body = "";
            if (Notification == "TotalHarga")
            {
                subject = "JegErs has Updated your Order Price!!";
                body = "Hi,<br/><br/>We have process your order with your information of clothes and total price.<br/><br/>" +
                    "Please process bank transfer and upload the transfer evidence so we can process your order as fast as we can.";
            }
            else if (Notification == "Finished")
            {
                if(delivery == "Yes")
                {
                    var query = (from a in db.Tbl_Order
                                where a.IDorder == idorder
                                select a).FirstOrDefault();
                    var query2 = (from a in db.Tbl_Orderdetail
                                 where a.IDorder == idorder
                                 select a).FirstOrDefault();
                    var queryitem = (from a in db.Tbl_Itemdetail
                                    where a.IDorder == idorder
                                    select a).ToList();

                    subject = "Yay!! Your Order is Finish and is on the way to your Home!! Thanks, We hope to see you again!!, JegErs";
                  
                    body = "Order Details<br/><br/><br/><br/>Order Date : " + query.Order_date + "<br/><br/>ID Order : " + query.IDorder +
                        "<br/><br/>Customer Name : " + query.Customer_Name + "<br/><br/>Total Kg Price : " + query2.Kg_Price.ToString() +
                        "<br/><br/>Total Special Price : " + query2.Total_Special_Price.ToString() + "<br/><br/>Total Delivery Price : " +
                        query2.Delivery_Price.ToString() + "<br/><br/><br/><br/>Total Price : " + query.Total_Price.ToString();

                    if (queryitem.Count > 0)
                    {
                        body += "<br/><br/><br/><br/><table><tr><th>Item Name</th><th>Measure</th><th>Amount</th><th>Service Type</th></tr>";
                        foreach (var item in queryitem)
                        {
                            body += "<tr><td>" + item.Item_Name + "</td><td>" + item.Measure + "</td><td>" + item.Amount.ToString() + "</td><td>" + item.Servicetype + "</td></tr>";
                        }
                        body += "</table>";
                    }
                }
                else if(delivery == "No")
                {
                    var query = (from a in db.Tbl_Order
                                 where a.IDorder == idorder
                                 select a).FirstOrDefault();
                    var query2 = (from a in db.Tbl_Orderdetail
                                  where a.IDorder == idorder
                                  select a).FirstOrDefault();
                    var queryitem = (from a in db.Tbl_Itemdetail
                                     where a.IDorder == idorder
                                     select a).ToList();

                    subject = "Yay!! Your Order is Finish and Ready for you to Take!! Thanks, We hope to see you again!!, JegErs";
                    body = "Order Details<br/><br/><br/><br/>Order Date : " + query.Order_date + "<br/><br/>ID Order : " + query.IDorder +
                        "<br/><br/>Customer Name : " + query.Customer_Name + "<br/><br/>Total Kg Price : " + query2.Kg_Price.ToString() +
                        "<br/><br/>Total Special Price : " + query2.Total_Special_Price.ToString() + "<br/><br/>Total Delivery Price : " +
                        query2.Delivery_Price.ToString() + "<br/><br/><br/><br/>Total Price : " + query.Total_Price.ToString();

                    if (queryitem.Count > 0)
                    {
                        body += "<br/><br/><br/><br/><table><tr><th>Item Name</th><th>Measure</th><th>Amount</th><th>Service Type</th></tr>";
                        foreach (var item in queryitem)
                        {
                            body += "<tr><td>" + item.Item_Name + "</td><td>" + item.Measure + "</td><td>" + item.Amount.ToString() + "</td><td>" + item.Servicetype + "</td></tr>";
                        }
                        body += "</table>";
                    }
                }
            }

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
    }
}