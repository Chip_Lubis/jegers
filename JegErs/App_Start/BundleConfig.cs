﻿using System.Web;
using System.Web.Optimization;

namespace JegErs
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/jquery-2.1.4.min.js",
                      "~/Scripts/modernizr-2.6.2.min.js",
                      "~/Scripts/jquery.zoomslider.min.js",
                      "~/Scripts/owl.carousel.js",
                      "~/Scripts/move-top.js",
                      "~/Scripts/easing.js",
                      "~/Scripts/datatables.min.js",
                      "~/Scripts/sweetalert.min.js",
                      "~/Scripts/thumbnailviewer.js",
                      "~/Scripts/jquery-ui.min.js",
                      "~/Scripts/nosorttable.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/zoomslider.css",
                      "~/Content/owl.carousel.css",
                      "~/Content/style.css",
                      "~/Content/datatables.min.css",
                      "~/Content/sweetalert.css",
                      "~/Content/thumbnailviewer.css",
                      "~/Content/jquery-ui.min.css"));

            bundles.Add(new StyleBundle("~/Content/cssHist").Include(
                      "~/Content/cssHist/bootstrap.css",
                      "~/Content/cssHist/font-awesome.css",
                      "~/Content/cssHist/ziehharmonika.css",
                      "~/Content/cssHist/style.css"));

            bundles.Add(new StyleBundle("~/Scripts/jsHist").Include(
                      "~/Scripts/jsHist/bootstrap.js",
                      "~/Scripts/jsHist/easing.js",
                      "~/Scripts/jsHist/jquery-2.1.4.min.js",
                      "~/Scripts/jsHist/move-top.js",
                      "~/Scripts/jsHist/numscroller-1.0.js",
                      "~/Scripts/jsHist/ziehharmonika.js"));
        }
    }
}
