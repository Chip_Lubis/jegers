//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JegErs
{
    using JegErs.Controllers;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using JegErs.Models;

    [MetadataType(typeof(Tbl_TermsAndRulesMetaData))]
    public partial class Tbl_TermsAndRules
    {
        public int id_termsandrules { get; set; }
        public string info_terms_rules { get; set; }
        public byte[] image_terms_rules { get; set; }
    }
}
